# hospitalityos

## Build Setup

```bash
# install dependencies
yarn install

# serve with hot reload at localhost:8080
yarn run dev

# build for production
yarn run build

# run production SSR server
yarn start
```
