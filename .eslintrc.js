module.exports = {
  root: true,
  extends: ['airbnb-base', 'plugin:vue/recommended', 'prettier'],
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: 'babel-eslint',
  },
  plugins: ['vue'],
  env: {
    'shared-node-browser': true,
  },
  settings: {
    'import/resolver': {
      webpack: {
        config: 'build/webpack.js',
      },
    },
  },
  rules: {
    'import/extensions': [
      'error',
      'always',
      {
        js: 'never',
        vue: 'never',
      },
    ],
    'arrow-body-style': 'off',
    camelcase: 'off',
    'no-underscore-dangle': 'off',
    'import/no-extraneous-dependencies': 'off',
    'no-use-before-define': ['error', 'nofunc'],
    'no-param-reassign': 'off',
    'vue/max-attributes-per-line': 'off',
    'vue/html-indent': 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
  },
  globals: {
    process: true,
  },
  overrides: [
    {
      files: ['server.js', 'dev-server.js', 'build/*.js', 'config/*.js'],
      env: {
        'shared-node-browser': false,
        node: true,
      },
      rules: {
        'no-console': 'off',
        'global-require': 'off',
      },
    },
    {
      files: ['src/**/*.test.js'],
      env: {
        jest: true,
      },
    },
  ],
}
