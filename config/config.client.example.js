const config = {
  publicPath: '/',
  apiUrl: '/wp-json',
}

module.exports = config
