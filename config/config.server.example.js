const config = {
  port: process.env.PORT || 8080,
  apiUrl: process.env.WP_API_URL || 'http://nginx/wp-json',
}

module.exports = config
