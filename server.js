const config = require('./config/config.server')
const fs = require('fs')
const path = require('path')
const express = require('express')
const { createBundleRenderer } = require('vue-server-renderer')

const template = fs.readFileSync('./src/template.html', 'utf-8')
const serverBundle = path.resolve(__dirname, 'dist/.server/vue-ssr-server-bundle.json')
const clientManifest = require('./dist/vue-ssr-client-manifest.json')

const renderer = createBundleRenderer(serverBundle, {
  runInNewContext: false,
  template,
  clientManifest,
})

const app = express()

const serve = file => express.static(path.join(__dirname, file))

app.use('/dist', serve('./dist'))
app.use('/service-worker.js', serve('./dist/service-worker.js'))

app.get(['/favicon.ico', '/robots.txt'], (req, res) => res.sendStatus(404))

// setup proxy for API calls
app.use(
  '/wp*',
  require('http-proxy-middleware')({
    target: new (require('url')).URL(config.apiUrl).origin,
    changeOrigin: true,
  }),
)

app.get('*', (req, res) => {
  res.setHeader('Content-Type', 'text/html')

  const handleError = err => {
    if (err && err.$statusCode === 404) {
      res.status(404).end('404 | Page Not Found')
    } else {
      // Render Error Page or Redirect
      res.status(500).end('500 | Internal Server Error')
      console.error(`error during render : ${req.url}`)
      console.error(err)
    }
  }

  const context = { req, res }

  renderer
    .renderToString(context)
    .then(html => {
      res.send(html)
    })
    .catch(handleError)
})

const { port, apiUrl } = config
app.listen(port, () => {
  console.log(`server started at port ${port}`)
  console.log(`apiUrl set to ${apiUrl}`)
})
