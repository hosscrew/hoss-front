FROM node:8.9

LABEL name="node-wordpress" \
      version="1.0" \
      maintainer="Angel Marin <marin@maniak.com.mx>"

RUN mkdir -p /var/app/

WORKDIR /var/app

ADD . .

RUN yarn install && \
    yarn run build

EXPOSE 8080

CMD ["yarn","start"]
