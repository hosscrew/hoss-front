module.exports = {
  presets: [
    [
      '@babel/preset-env',
      {
        modules: process.env.NODE_ENV === 'test' ? 'commonjs' : false,
        loose: true,
        useBuiltIns: 'usage',
      },
    ],
    '@babel/preset-stage-3',
  ],
  plugins: ['transform-vue-jsx'],
}
