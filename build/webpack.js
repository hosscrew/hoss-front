const webpack = require('webpack')
const path = require('path')
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

const isProd = process.env.NODE_ENV === 'production'
const root = path.resolve(__dirname, '..')

const webpackConfig = {
  mode: isProd ? 'production' : 'development',
  output: {
    path: path.resolve(root, 'dist'),
    filename: isProd ? '[name].[chunkhash].js' : '[name].js',
    chunkFilename: isProd ? '[id].[chunkhash].js' : undefined,
    publicPath: '/dist/',
    jsonpFunction: '_$hos',
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['vue-style-loader', 'css-loader'],
      },
      {
        test: /\.scss$/,
        use: ['vue-style-loader', 'css-loader', 'sass-loader'],
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          extractCSS: isProd,
          preserveWhitespace: false,
          loaders: {
            js: babelLoader(),
          },
        },
      },
      {
        test: /\.js$/,
        exclude: /node_modules\/(?!(dom7|swiper)\/).*/,
        ...babelLoader(),
      },
      {
        test: /\.(png|jpg|jpeg|gif|eot|ttf|woff|woff2|svg|svgz)(\?.+)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000,
              name: '[name].[hash].[ext]',
            },
          },
        ],
      },
    ],
  },
  resolve: {
    extensions: ['*', '.js', '.vue', '.json'],
    alias: {
      '@core': path.resolve(root, 'src/core'),
      vue$: 'vue/dist/vue.esm.js',
      swiper$: 'swiper/dist/js/swiper.esm.js',
    },
  },
  plugins: [
    new FriendlyErrorsPlugin(),

    // compat for webpack 4
    new webpack.LoaderOptionsPlugin({
      options: {
        context: process.cwd(),
      },
    }),

    new ExtractTextPlugin({ disable: !isProd, filename: 'main.[chunkhash].css' }),
  ],
  node: {
    // prevent webpack from injecting useless setImmediate polyfill because Vue
    // source contains it (although only uses it if it's native).
    setImmediate: false,
    // prevent webpack from injecting mocks to Node native modules
    // that does not make sense for the client
    dgram: 'empty',
    fs: 'empty',
    net: 'empty',
    tls: 'empty',
    child_process: 'empty',
  },
}

function babelLoader() {
  return {
    loader: 'babel-loader',
    options: {
      ...require('../.babelrc'),
      babelrc: false,
    },
  }
}

module.exports = webpackConfig
