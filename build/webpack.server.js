const path = require('path')
const merge = require('webpack-merge')
const webpack = require('webpack')
const nodeExternals = require('webpack-node-externals')
const VueSSRServerPlugin = require('vue-server-renderer/server-plugin')
const baseWebpackConfig = require('./webpack')

const root = path.resolve(__dirname, '..')

const webpackConfig = merge(baseWebpackConfig, {
  target: 'node',

  entry: path.resolve(root, 'src/entry.server.js'),

  devtool: '#source-map',

  output: {
    path: path.resolve(root, 'dist/.server'),
    filename: 'server.bundle.js',
    libraryTarget: 'commonjs2',
  },

  externals: [
    (context, request, callback) => {
      if (/config.server(\.js)?$/i.test(request))
        // rely on the fact that server bundle is executed in vue-server-renderer dir context,
        // which is located at <roo>/node_modules/vue-server-renderer, using this ugly hack below
        // we require the apps config from within that directory
        // final require call in bundle: `require("../../config/config.server")`
        callback(null, 'commonjs ../../config/config.server')
      else callback()
    },
    nodeExternals({
      // do not externalize dependencies that need to be processed by webpack.
      // you can add more file types here e.g. raw *.vue files
      // you should also whitelist deps that modifies `global` (e.g. polyfills)
      whitelist: [/\.s?css$/, /^lodash-es/, /^swiper/, /^dom7/],
    }),
  ],

  plugins: [
    new webpack.DefinePlugin({
      'process.env.VUE_ENV': JSON.stringify('server'),
    }),
    new VueSSRServerPlugin(),
  ],

  performance: {
    hints: false,
  },
})

module.exports = webpackConfig
