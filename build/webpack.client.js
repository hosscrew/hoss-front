const path = require('path')
const merge = require('webpack-merge')
const webpack = require('webpack')
const SWPrecachePlugin = require('sw-precache-webpack-plugin')
const VueSSRClientPlugin = require('vue-server-renderer/client-plugin')
const baseWebpackConfig = require('./webpack')

const root = path.resolve(__dirname, '..')
const isProd = process.env.NODE_ENV === 'production'

const webpackConfig = merge(baseWebpackConfig, {
  entry: path.resolve(root, 'src/entry.client.js'),

  devtool: !isProd && '#cheap-module-eval-source-map',

  optimization: {
    splitChunks: {
      chunks: 'all',
    },
    runtimeChunk: true,
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env.VUE_ENV': JSON.stringify('client'),
    }),
    // This plugins generates `vue-ssr-client-manifest.json` in the
    // output directory.
    new VueSSRClientPlugin(),
  ],

  performance: {
    maxEntrypointSize: 1024 * 300,
    hints: isProd ? 'warning' : false,
  },
})

if (isProd) {
  webpackConfig.plugins = webpackConfig.plugins.concat([
    // auto generate service worker
    new SWPrecachePlugin({
      cacheId: 'mnk',
      filename: 'service-worker.js',
      minify: true,
      dontCacheBustUrlsMatching: /./,
      staticFileGlobsIgnorePatterns: [/index\.html$/, /\.map$/],
    }),
  ])
}

if (process.env.ANALYZE) {
  const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')

  webpackConfig.plugins = webpackConfig.plugins.concat([new BundleAnalyzerPlugin()])
}

module.exports = webpackConfig
