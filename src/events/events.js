// Getters
import makePostTypeStore from '../core/post-types/post-types'

export const eventsById = 'eventsById'
export const eventById = 'eventById'
export const eventBySlug = 'eventBySlug'
export const eventErrorBySlug = 'eventErrorBySlug'
export const eventInCurrentRoute = 'eventInCurrentRoute'
export const eventErrorInCurrentRoute = 'eventErrorInCurrentRoute'

// Actions
export const requestEventBySlug = 'requestEventBySlug'

// Mutations
export const requestEventStarted = 'requestEventStarted'
export const requestEventFinished = 'requestEventFinished'

const events = makePostTypeStore({
  name: 'event',
  restBase: 'events',

  // Getters
  postTypesById: eventsById,
  postTypeById: eventById,
  postTypeBySlug: eventBySlug,
  postTypeErrorBySlug: eventErrorBySlug,
  postTypeInCurrentRoute: eventInCurrentRoute,
  postTypeErrorInCurrentRoute: eventErrorInCurrentRoute,

  // Actions
  requestPostTypeBySlug: requestEventBySlug,

  // Mutations
  requestPostTypeStarted: requestEventStarted,
  requestPostTypeFinished: requestEventFinished,
})

export default events
