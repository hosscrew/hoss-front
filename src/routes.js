import Vue from 'vue'
import VueRouter from 'vue-router'
import PageRoute from './core/pages/PageRoute'
import EventRoute from './events/EventRoute'

Vue.use(VueRouter)

const createRouter = () =>
  new VueRouter({
    mode: 'history',
    scrollBehavior() {
      return { x: 0, y: 0 }
    },
    routes: [
      {
        path: '/events/:slug',
        component: EventRoute,
      },
      {
        path: '/:slug',
        component: PageRoute,
      },
    ],
  })

export default createRouter
