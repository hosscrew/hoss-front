import flatMap from 'lodash-es/flatMap'
import maxBy from 'lodash-es/maxBy'
import Vue from 'vue'
import isArray from 'lodash-es/isArray'
import config from '../config/config.server'
import { API_URL } from './core/catalog'
import { $req } from './core/catalog.server'
import http from './core/services/http/http.server'
import createApp from './create-app'
import createDiContainer from './services/create-di-container'
import { notFound } from './core/error'

// This exported function will be called by `bundleRenderer`.
// This is where we perform data-prefetching to determine the
// state of our application before actually rendering it.
// Since data fetching is async, this function is expected to
// return a Promise that resolves to the app instance.
export default context => {
  return new Promise((resolve, reject) => {
    const container = createDiContainer()

    container.constant(API_URL, config.apiUrl)

    container.constant($req, context.req)

    container.register(http)

    const { app, store, router } = createApp(container)

    router.push(context.req.url)

    router.onReady(() => {
      const matchedComponents = router.getMatchedComponents()

      if (!matchedComponents.length) {
        reject(notFound('Route not found'))
        return
      }

      // Call fetchData hooks on components matched by the route.
      // A preFetch hook dispatches a store action and returns a Promise,
      // which is resolved when the action is complete and store state has been
      // updated.
      Promise.all(
        flatMap(matchedComponents, Component => {
          const { asyncData } = Vue.extend(Component).options

          return (
            asyncData &&
            (isArray(asyncData) ? asyncData : [asyncData]).map(request =>
              request({
                store,
                route: router.currentRoute,
              }),
            )
          )
        }),
      )
        .then(asyncResults => {
          const result = maxBy(asyncResults, res => res && res.$statusCode)

          if (result) {
            context.res.status(result.$statusCode)
          }

          // After all preFetch hooks are resolved, our store is now
          // filled with the state needed to render the app.
          // Expose the state on the render context, and let the request handler
          // inline the state in the HTML response. This allows the client-side
          // store to pick-up the server-side state without having to duplicate
          // the initial data fetching on the client.
          context.state = store.state

          resolve(app)
        })
        .catch(reject)
    }, reject)
  })
}
