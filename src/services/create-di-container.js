import * as Bottle from 'bottlejs'
import kebabCase from 'lodash-es/kebabCase'
import mapKeys from 'lodash-es/mapKeys'
import {
  $mediaComponents,
  $routeErrorComponent,
  $userComponents,
  $wysiwygComponents,
} from '@core/catalog'
import Value from '@core/components/Value'
import CloudinaryImage from '@core/components/media/CloudinaryImage'
import WpVideo from '@core/components/media/WpVideo'
import RouteError from '@core/components/RouteError'
import Hero from '../components/cachet/Hero'
import SingleText from '../components/cachet/SingleText'
import TriContent from '../components/cachet/TriContent'

Bottle.config.strict = true

/**
 * Common DI container for client and server
 * @return {Bottle}
 */
const createDiContainer = () => {
  const container = new Bottle()

  container.factory($userComponents, () => ({
    OsButton: () => import('../components/OsButton'),
    complex_component: () => import('../components/ComplexComponent'),
    slider: () => import('../components/AcfSlider'),
    custom_2_image_slider: () => import('../components/TwoImageSlider'),

    hero: Hero,
    single_text: SingleText,
    tri_content: TriContent,
  }))

  container.factory($mediaComponents, () => ({
    image: CloudinaryImage,
    video: WpVideo,
  }))

  container.factory($wysiwygComponents, deps => {
    // Enable all acf-level components for wysiwyg
    return {
      ...mapKeys(deps[$userComponents], (comp, compName) => kebabCase(compName)),
      value: Value,
    }
  })

  container.constant($routeErrorComponent, RouteError)

  return container
}

export default createDiContainer
