import Vue from 'vue'
import { sync } from 'vuex-router-sync'
import corePlugin from '@core/vue-plugin'
import App from './App'
import DiProvider from './core/components/DiProvider'
import createStore from './create-store'
import createRouter from './routes'

Vue.use(corePlugin)

const createApp = diContainer => {
  const store = createStore(diContainer)
  const router = createRouter(diContainer)

  sync(store, router)

  const app = new Vue({
    router,
    store,
    render() {
      return (
        <DiProvider container={diContainer}>
          <App />
        </DiProvider>
      )
    },
  })

  return { app, store, router }
}

export default createApp
