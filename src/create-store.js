import Vue from 'vue'
import Vuex from 'vuex'
import settingsModule from '@core/settings/settings'
import menus from '@core/menus/menus'
import inject from './core/store/inject-plugin'
import pages from './core/pages/pages'
import events from './events/events'

Vue.use(Vuex)

const createStore = diContainer => {
  return new Vuex.Store({
    strict: process.env.NODE_ENV !== 'production',
    modules: {
      settings: settingsModule,
      menus,
      pages,
      events,
    },
    plugins: [inject(diContainer)],
  })
}

export default createStore
