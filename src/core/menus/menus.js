import keyBy from 'lodash-es/keyBy'
import { e } from '../error'

// Getters
export const menuBySlug = 'menuBySlug'
export const menusError = 'menusError'

// Actions
export const requestMenus = 'requestMenus'

// Mutations
export const requestMenusFinished = 'requestMenusFinished'

const menus = {
  state() {
    return {
      bySlug: {},
      error: null,
    }
  },

  getters: {
    [menuBySlug]: ({ bySlug }) => ({ slug }) => bySlug[slug],

    [menusError]: ({ error }) => error,
  },

  actions: {
    async [requestMenus]({ commit }) {
      try {
        const res = await this.$http.get(`/hos/v1/menus`)
        commit(requestMenusFinished, { res: res.data })
      } catch (error) {
        commit(requestMenusFinished, { error })
      }
    },
  },

  mutations: {
    [requestMenusFinished](state, { res, error }) {
      if (res) {
        state.bySlug = keyBy(res, 'slug')
      } else if (error) {
        state.error = e(error)
      }
    },
  },
}

export default menus
