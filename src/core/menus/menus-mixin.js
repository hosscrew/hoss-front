import { mapGetters } from 'vuex'
import { menuBySlug, menusError, requestMenus } from '@core/menus/menus'

const settings = {
  asyncData: ({ store }) => {
    return store.dispatch(requestMenus).then(() => store.getters[menusError])
  },
  computed: mapGetters({
    menuBySlug,
  }),
}

export default settings
