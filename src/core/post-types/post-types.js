import mapKeys from 'lodash-es/mapKeys'
import Vue from 'vue'
import { e, notFound } from '../error'

const makePostTypeStore = ({
  name,
  restBase,

  // Getters
  postTypesById,
  postTypeById,
  postTypeBySlug,
  postTypeErrorBySlug,
  postTypeInCurrentRoute,
  postTypeErrorInCurrentRoute,

  // Actions
  requestPostTypeBySlug,

  // Mutations
  requestPostTypeStarted,
  requestPostTypeFinished,
}) => {
  return {
    state() {
      return {
        bySlug: {},
        loading: {},
      }
    },

    getters: {
      [postTypesById]: ({ bySlug }) => mapKeys(bySlug, post => post.id),

      [postTypeById]: (state, { [postTypesById]: byId }) => ({ id }) => byId[id],

      [postTypeBySlug]: ({ bySlug }) => ({ slug }) => bySlug[slug],

      [postTypeErrorBySlug]: ({ loading }) => ({ slug }) =>
        loading[slug] ? loading[slug].error : null,

      [postTypeInCurrentRoute]: (state, { [postTypeBySlug]: bySlug }, { route }) =>
        bySlug(route.params),

      [postTypeErrorInCurrentRoute]: (state, { [postTypeErrorBySlug]: errorBySlug }, { route }) =>
        errorBySlug(route.params),
    },

    actions: {
      async [requestPostTypeBySlug]({ commit }, params) {
        commit(requestPostTypeStarted, params)

        try {
          const res = await this.$http.get(`/wp/v2/${restBase}`, { params: { slug: params.slug } })
          if (!res.data || res.data.length < 1) {
            commit(requestPostTypeFinished, { error: notFound(`${name} not found`), params })
          } else {
            commit(requestPostTypeFinished, { postType: res.data[0], params })
          }
        } catch (error) {
          commit(requestPostTypeFinished, { error, params })
        }
      },
    },

    mutations: {
      [requestPostTypeStarted]({ loading }, { slug }) {
        Vue.set(loading, slug, {
          isLoading: true,
          error: null,
        })
      },

      [requestPostTypeFinished]({ bySlug, loading }, { params: { slug }, postType, error }) {
        if (postType) {
          Vue.set(bySlug, slug, Object.freeze(postType))
          Vue.delete(loading, slug)
        } else if (error) {
          Vue.set(loading, slug, {
            isLoading: false,
            error: e(error),
          })
        }
      },
    },
  }
}

export default makePostTypeStore
