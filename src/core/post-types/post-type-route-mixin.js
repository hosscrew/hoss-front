import { mapGetters } from 'vuex'

const makePostTypeRouteMixin = ({
  name,
  postTypeErrorBySlug,
  postTypeErrorInCurrentRoute,
  postTypeInCurrentRoute,
  requestPostTypeBySlug,
}) => {
  return {
    asyncData: ({ store, route }) => {
      return store
        .dispatch(requestPostTypeBySlug, route.params)
        .then(() => store.getters[postTypeErrorBySlug](route.params))
    },
    computed: {
      ...mapGetters({
        [name]: postTypeInCurrentRoute,
        error: postTypeErrorInCurrentRoute,
      }),
    },
  }
}

export default makePostTypeRouteMixin
