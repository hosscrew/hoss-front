import { $http, API_URL } from '../../catalog'
import { $req } from '../../catalog.server'
import createAxiosInstance from './axios'

const http = (baseUrl, req) => {
  const axios = createAxiosInstance(baseUrl)

  if (req.headers.cookie) {
    axios.defaults.headers.common = {
      Cookie: req.headers.cookie,
    }
  }

  return axios
}

Object.assign(http, {
  $name: $http,
  $inject: [API_URL, $req],
})

export default http
