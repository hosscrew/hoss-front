import { $http, API_URL } from '../../catalog'
import createAxiosInstance from './axios'

const http = baseUrl => {
  const axios = createAxiosInstance(baseUrl)

  axios.interceptors.request.use(
    config => config,
    error => {
      if (process.env.NODE_ENV !== 'production') console.error('axios error', error)

      return error
    },
  )

  return axios
}

Object.assign(http, {
  $name: $http,
  $inject: [API_URL],
})

export default http
