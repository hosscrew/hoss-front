import settings from '@core/settings/settings-mixin'
import menus from '@core/menus/menus-mixin'

const routeEntry = {
  mixins: [settings, menus],
}

export default routeEntry
