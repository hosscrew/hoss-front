import identity from 'lodash-es/identity'

const findAsyncData = ({ asyncData, mixins = [] }) => {
  const all = mixins.map(mixin => mixin.asyncData).filter(identity)

  if (asyncData) all.push(asyncData)

  return all
}

export default findAsyncData
