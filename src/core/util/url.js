/* eslint-disable global-require */
if (process.env.VUE_ENV === 'server') module.exports = require('url-parse')
else module.exports = window.URL
