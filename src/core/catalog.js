export const API_URL = '$API_URL'

// HTTP client (axios compatible)
export const $http = '$http'

// Vuex store
export const $store = '$store'

// User-level component catalog
export const $userComponents = '$userComponents'

// Media component catalog
export const $mediaComponents = '$mediaComponents'

// WYSIWYG-enabled components
export const $wysiwygComponents = '$wysiwygComponents'

// Route error component for 4xx and 5xx errors
export const $routeErrorComponent = '$routeErrorComponent'
