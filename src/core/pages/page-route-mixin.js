import {
  pageErrorBySlug,
  pageErrorInCurrentRoute,
  pageInCurrentRoute,
  requestPageBySlug,
} from './pages'
import makePostTypeRouteMixin from '../post-types/post-type-route-mixin'

const pageRoute = makePostTypeRouteMixin({
  name: 'page',
  postTypeErrorBySlug: pageErrorBySlug,
  postTypeErrorInCurrentRoute: pageErrorInCurrentRoute,
  postTypeInCurrentRoute: pageInCurrentRoute,
  requestPostTypeBySlug: requestPageBySlug,
})

export default pageRoute
