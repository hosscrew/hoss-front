import makePostTypeStore from '../post-types/post-types'

// Getters
export const pagesById = 'pagesById'
export const pageById = 'pageById'
export const pageBySlug = 'pageBySlug'
export const pageErrorBySlug = 'pageErrorBySlug'
export const pageInCurrentRoute = 'pageInCurrentRoute'
export const pageErrorInCurrentRoute = 'pageErrorInCurrentRoute'

// Actions
export const requestPageBySlug = 'requestPageBySlug'

// Mutations
export const requestPageStarted = 'requestPageStarted'
export const requestPageFinished = 'requestPageFinished'

const pages = makePostTypeStore({
  name: 'page',
  restBase: 'pages',

  // Getters
  postTypesById: pagesById,
  postTypeById: pageById,
  postTypeBySlug: pageBySlug,
  postTypeErrorBySlug: pageErrorBySlug,
  postTypeInCurrentRoute: pageInCurrentRoute,
  postTypeErrorInCurrentRoute: pageErrorInCurrentRoute,

  // Actions
  requestPostTypeBySlug: requestPageBySlug,

  // Mutations
  requestPostTypeStarted: requestPageStarted,
  requestPostTypeFinished: requestPageFinished,
})

export default pages
