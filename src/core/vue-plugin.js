const plugin = {
  install(Vue) {
    Vue.config.optionMergeStrategies.asyncData = Vue.config.optionMergeStrategies.beforeCreate
  },
}

export default plugin
