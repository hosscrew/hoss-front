import { e } from '../error'

// Getters
export const settings = 'settings'
export const settingByKey = 'settingByKey'
export const settingsError = 'settingsError'

// Actions
export const requestSettings = 'requestSettings'

// Mutations
export const requestSettingsFinished = 'requestSettingsFinished'

const settingsModule = {
  state() {
    return {
      dict: {},
      error: null,
    }
  },

  getters: {
    [settings]: state => state.dict,

    [settingByKey]: ({ dict }) => key => dict[key],

    [settingsError]: ({ error }) => error,
  },

  actions: {
    async [requestSettings]({ commit }) {
      try {
        const res = await this.$http.get(`/wp/v2/settings`)
        commit(requestSettingsFinished, { res: res.data })
      } catch (error) {
        commit(requestSettingsFinished, { error })
      }
    },
  },

  mutations: {
    [requestSettingsFinished](state, { res, error }) {
      if (res) {
        state.dict = res
      } else if (error) {
        state.error = e(error)
      }
    },
  },
}

export default settingsModule
