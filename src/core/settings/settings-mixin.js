import { mapGetters } from 'vuex'
import { requestSettings, settingByKey, settings, settingsError } from '@core/settings/settings'

const settingsMixin = {
  asyncData: ({ store }) => {
    return store.dispatch(requestSettings).then(() => store.getters[settingsError])
  },
  computed: mapGetters({
    settings,
    settingByKey,
  }),
}

export default settingsMixin
