import serializeError from 'serialize-error'
import toString from 'lodash-es/toString'
import get from 'lodash-es/get'
import pick from 'lodash-es/pick'

const errorTag = '$__isErrorX'

const httpTag = '$__isHttpx'

export const e = (original, details) => {
  if (original && original[errorTag]) return Object.assign(original, details)

  let err
  let $statusCode = 500

  if (original instanceof Error) {
    err = original

    const httpStatus = get(err, 'response.status')

    if (httpStatus) {
      $statusCode = httpStatus
      Object.defineProperty(err, httpTag, {
        enumerable: false,
        configurable: false,
        writable: false,
        value: true,
      })
    }
  } else {
    const msg = toString(original)
    err = new Error(msg.startsWith('[object') ? undefined : msg)
    err.$original = original

    if (Error.captureStackTrace) {
      Error.captureStackTrace(err, e)
    }
  }

  Object.assign(
    err,
    {
      $timestamp: Date.now(),
      $statusCode,
    },
    details,
  )

  Object.defineProperty(err, errorTag, {
    enumerable: true,
    configurable: false,
    writable: false,
    value: true,
  })

  err.toJSON = function toJSON() {
    const shallow = { ...this }

    if (process.env.NODE_ENV === 'production' && process.env.VUE_ENV === 'server') {
      // ensure we don't leak stack traces to the front-end
      delete shallow.stack

      // don't leak server http client configuration
      if (this[httpTag]) {
        delete shallow.config
        delete shallow.request

        shallow.response = pick(shallow.response, 'data', 'status', 'statusText')
      }
    } else {
      shallow.stack = this.stack
    }

    return serializeError(shallow)
  }

  return err
}

export const notFound = (msg = 'NotFound', details) => {
  const err = new Error(msg)

  if (Error.captureStackTrace) {
    Error.captureStackTrace(err, notFound)
  }

  return e(err, Object.assign({ $statusCode: 404 }, details))
}
