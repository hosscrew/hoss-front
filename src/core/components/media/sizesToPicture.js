import isString from 'lodash-es/isString'
import flatMap from 'lodash-es/flatMap'
import round from 'lodash-es/round'
import groupBy from 'lodash-es/groupBy'
import sortBy from 'lodash-es/sortBy'
import findIndex from 'lodash-es/findIndex'
import compact from 'lodash-es/compact'

export const parseSizes = sizes => {
  const parser = /^\s*(\(.+?\)(?=\s+[\w\d])(?!\s*and|\s*or))??\s*([^(].+?)(?:\s+(?:(\d+)\/(\d+)|(\d+(?:\.\d+)?\w+)))?\s*$/

  return sizes.split(',').map(segment => {
    const ret = parser.exec(segment)

    if (process.env.NODE_ENV !== 'production' && !ret)
      throw new Error(`invalid sizes segment: ${segment}`)

    const [, media, width, arWidth, arHeight, height] = ret

    const aspectRatio = height
      ? undefined
      : Number.parseInt(arWidth, 10) / Number.parseInt(arHeight, 10)

    return { media, width, aspectRatio, height }
  })
}

const nearestAspectRatio = (ratios, target) => {
  const winner = ratios.reduce(
    (prev, curr) =>
      Math.abs(curr.arValue - target) < Math.abs(prev.arValue - target) ? curr : prev,
  )

  return { ...winner, index: ratios.indexOf(winner) }
}

const ratioActivationMedia = (height, width, heightUnit, ratio) => {
  if (heightUnit === 'vh')
    return `(max-aspect-ratio: ${round(height / 100 * ratio * (width / 100) * 1000)}/1000)`

  if (heightUnit === 'px') return `(max-width: ${round(height * ratio / (width / 100))}px)`

  throw new Error(`unsupported unit ${heightUnit}`)
}

export const enrichSizesWithAspectRatios = (sizes, availableRatios) =>
  flatMap(sizes, ({ media, width, height, aspectRatio }) => {
    if (aspectRatio) {
      if (process.env.NODE_ENV !== 'production' && height)
        throw new Error('height and aspect ratio are mutually exclusive')

      const { arString, index } = nearestAspectRatio(availableRatios, aspectRatio)

      return {
        media,
        width,
        aspectRatio: arString,
        aspectRatioIdx: index,
      }
    }

    if (height) {
      const heightParser = /(\d+(?:\.\d+)?)(vh|px)/

      if (process.env.NODE_ENV !== 'production') {
        if (!/\d+(\.\d+)?vw/.test(width))
          throw new Error('width must be simple a vw dimension when also specifying height')

        if (!heightParser.test(height))
          throw new Error('height must be a simple vh or px dimension when specified')
      }

      const [, heightStr, unit] = heightParser.exec(height)
      const targetHeight = Number.parseInt(heightStr, 10)
      const targetWidth = Number.parseInt(width, 10)
      const sources = []

      const lastRatio = availableRatios.reduce((currRatio, nextRatio, i) => {
        const ratioExtent = (currRatio.arValue + nextRatio.arValue) / 2

        sources.push({
          media,
          width,
          aspectRatio: currRatio.arString,
          aspectRatioIdx: i - 1,
          injectedMedia: ratioActivationMedia(targetHeight, targetWidth, unit, ratioExtent),
        })

        return nextRatio
      })

      sources.push({
        media,
        width,
        aspectRatio: lastRatio.arString,
        aspectRatioIdx: availableRatios.length - 1,
      })

      return sources
    }

    return {
      media,
      width,
      aspectRatio: null,
      aspectRatioIdx: availableRatios.length - 1,
    }
  })

// moxy h:  (min-width: 1024px) 100vw 100vh, (min-width: 640) 100vw 70vh, 100vw 375px
// hero:    100vw 100vh
// tri:     (min-width: 640px)  calc(33.33vw - 70px - 2vw) 3/4, calc(100vw - 70px) 3/4
// gallery: (min-width: 40px)   100vw 1000/534, 100vw 1000/835
// offers:  (min-width: 1100px) calc(50vw - 60px) 1/1, (min-width: 1090px) calc(50vw - 60px) 3/4, calc(100vw - 54px) 1/1
// collage: (min-width: 640px) 50vw 1000/1064 , 50vw 1000/1403
const sizesToPicture = (sizes, availableRatios) => {
  if (isString(sizes)) sizes = parseSizes(sizes)

  availableRatios = sortBy(availableRatios, r => r.arValue)

  const transformed = enrichSizesWithAspectRatios(sizes, availableRatios)

  const grouped = sortBy(groupBy(transformed, s => s.aspectRatio), s => s[0].aspectRatioIdx)

  const sources = grouped.map(group => {
    // TODO implement deduping
    // FIXME queries can overlap since it not safe to reorder them, try to add constrains
    //       by looking at previous breakpoint (min/max-width) and restrict accordingly (fragile?)

    const { media, sizesArr } = group.reduce(
      (s, g) => {
        const finalMedia = compact([g.media, g.injectedMedia]).join(' and ')

        if (finalMedia) s.media.push(finalMedia)

        s.sizesArr.push(g.media ? `${g.media} ${g.width}` : g.width)

        return s
      },
      {
        media: [],
        sizesArr: [],
      },
    )

    return {
      media: media.join(', '),
      sizes: sizesArr.join(', '),
      srcset: availableRatios[group[0].aspectRatioIdx].srcset.join(', '),
    }
  })

  let imgIdx = findIndex(sources, s => !s.media)
  imgIdx = imgIdx !== -1 ? imgIdx : sources.length - 1

  return {
    sources: [...sources.slice(0, imgIdx), ...sources.slice(imgIdx + 1)],
    img: sources[imgIdx],
  }
}

export default sizesToPicture
