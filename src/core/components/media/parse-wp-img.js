import reduce from 'lodash-es/reduce'

const parseWpImg = wpImg => ({
  src: wpImg.url,
  width: wpImg.width,
  height: wpImg.height,
  alt: wpImg.alt || wpImg.title || wpImg.description || wpImg.name || wpImg.caption,
  srcset: buildSrcset(wpImg.sizes),
  sources:
    wpImg.sources &&
    wpImg.sources.map(({ media }) => ({
      media,
      srcset: buildSrcset(media.sizes),
    })),
})

function buildSrcset(sources) {
  return reduce(
    sources,
    (srcset, maybeUrl, maybeSrc) => {
      if (!/-(width|height)$/.test(maybeSrc))
        srcset.push(`${maybeUrl} ${sources[`${maybeSrc}-width`]}w`)

      return srcset
    },
    [],
  ).join(', ')
}

export default parseWpImg
