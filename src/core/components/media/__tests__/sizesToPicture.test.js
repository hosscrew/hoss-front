import sizesToPicture, { enrichSizesWithAspectRatios } from '../sizesToPicture'

const fullAspectRatioSource = Object.freeze([
  { arValue: 9 / 16, arString: '9/16', srcset: ['9/16.jpg 100w'] },
  { arValue: 3 / 4, arString: '3/4', srcset: ['3/4.jpg 100w'] },
  { arValue: 1, arString: '1/1', srcset: ['1/1.jpg 100w'] },
  { arValue: 4 / 3, arString: '4/3', srcset: ['4/3.jpg 100w'] },
  { arValue: 16 / 9, arString: '16/9', srcset: ['16/9.jpg 100w'] },
])

describe('enrichSizesWithAspectRatios', () => {
  it('returns empty array with no params', () => {
    expect(enrichSizesWithAspectRatios()).toEqual([])
  })

  it('parses `100vw 100vh` with one source ratio', () => {
    expect(
      enrichSizesWithAspectRatios(
        [
          {
            width: '100vw',
            height: '100vh',
          },
        ],
        [{ arValue: 16 / 9, arString: '16/9' }],
      ),
    ).toMatchObject([
      {
        width: '100vw',
        aspectRatio: '16/9',
      },
    ])
  })

  it('parses `100vw 100vh`', () => {
    expect(
      enrichSizesWithAspectRatios(
        [
          {
            width: '100vw',
            height: '100vh',
          },
        ],
        fullAspectRatioSource,
      ),
    ).toMatchObject([
      {
        width: '100vw',
        aspectRatio: '9/16',
        injectedMedia: '(max-aspect-ratio: 656/1000)',
      },
      {
        width: '100vw',
        aspectRatio: '3/4',
        injectedMedia: '(max-aspect-ratio: 875/1000)',
      },
      {
        width: '100vw',
        aspectRatio: '1/1',
        injectedMedia: '(max-aspect-ratio: 1167/1000)',
      },
      {
        width: '100vw',
        aspectRatio: '4/3',
        injectedMedia: '(max-aspect-ratio: 1556/1000)',
      },
      {
        width: '100vw',
        aspectRatio: '16/9',
      },
    ])
  })

  it('parses (min-width: 1024px) 100vw 100vh, (min-width: 640px) 100vw 70vh, 100vw 375px', () => {
    expect(
      enrichSizesWithAspectRatios(
        [
          {
            width: '100vw',
            height: '100vh',
            media: '(min-width: 1024px)',
          },
          {
            width: '100vw',
            height: '70vh',
            media: '(min-width: 640px)',
          },
          {
            width: '100vw',
            height: '375px',
          },
        ],
        fullAspectRatioSource,
      ),
    ).toMatchObject([
      {
        aspectRatio: '9/16',
        injectedMedia: '(max-aspect-ratio: 656/1000)',
        media: '(min-width: 1024px)',
        width: '100vw',
      },
      {
        aspectRatio: '3/4',
        injectedMedia: '(max-aspect-ratio: 875/1000)',
        media: '(min-width: 1024px)',
        width: '100vw',
      },
      {
        aspectRatio: '1/1',
        injectedMedia: '(max-aspect-ratio: 1167/1000)',
        media: '(min-width: 1024px)',
        width: '100vw',
      },
      {
        aspectRatio: '4/3',
        injectedMedia: '(max-aspect-ratio: 1556/1000)',
        media: '(min-width: 1024px)',
        width: '100vw',
      },
      { aspectRatio: '16/9', media: '(min-width: 1024px)', width: '100vw' },
      {
        aspectRatio: '9/16',
        injectedMedia: '(max-aspect-ratio: 459/1000)',
        media: '(min-width: 640px)',
        width: '100vw',
      },
      {
        aspectRatio: '3/4',
        injectedMedia: '(max-aspect-ratio: 612/1000)',
        media: '(min-width: 640px)',
        width: '100vw',
      },
      {
        aspectRatio: '1/1',
        injectedMedia: '(max-aspect-ratio: 817/1000)',
        media: '(min-width: 640px)',
        width: '100vw',
      },
      {
        aspectRatio: '4/3',
        injectedMedia: '(max-aspect-ratio: 1089/1000)',
        media: '(min-width: 640px)',
        width: '100vw',
      },
      { aspectRatio: '16/9', media: '(min-width: 640px)', width: '100vw' },
      {
        aspectRatio: '9/16',
        injectedMedia: '(max-width: 246px)',
        media: undefined,
        width: '100vw',
      },
      {
        aspectRatio: '3/4',
        injectedMedia: '(max-width: 328px)',
        media: undefined,
        width: '100vw',
      },
      {
        aspectRatio: '1/1',
        injectedMedia: '(max-width: 437px)',
        media: undefined,
        width: '100vw',
      },
      {
        aspectRatio: '4/3',
        injectedMedia: '(max-width: 583px)',
        media: undefined,
        width: '100vw',
      },
      { aspectRatio: '16/9', media: undefined, width: '100vw' },
    ])
  })

  it('parses `(min-width: 640px) calc(33.33vw - 70px - 2vw) 3/4, calc(100vw - 70px) 3/4`', () => {
    expect(
      enrichSizesWithAspectRatios(
        [
          {
            width: 'calc(33.33vw - 70px - 2vw)',
            media: '(min-width: 640px)',
            aspectRatio: 3 / 4,
          },
          {
            width: 'calc(100vw - 70px)',
            aspectRatio: 3 / 4,
          },
        ],
        fullAspectRatioSource,
      ),
    ).toMatchObject([
      {
        aspectRatio: '3/4',
        media: '(min-width: 640px)',
        width: 'calc(33.33vw - 70px - 2vw)',
      },
      { aspectRatio: '3/4', media: undefined, width: 'calc(100vw - 70px)' },
    ])
  })
})

describe('sizesToPicture', () => {
  it('transforms `(min-width: 640px) 50vw 1000/1064 , 50vw 1000/1403`', () => {
    expect(
      sizesToPicture('(min-width: 640px) 50vw 1000/1064, 50vw 1000/1403', fullAspectRatioSource),
    ).toMatchObject({
      img: {
        srcset: '3/4.jpg 100w',
        sizes: '50vw',
      },
      sources: [
        {
          media: '(min-width: 640px)',
          srcset: '1/1.jpg 100w',
          sizes: '(min-width: 640px) 50vw',
        },
      ],
    })
  })

  it('transforms `(min-width: 640px)  calc(33.33vw - 70px - 2vw) 3/4, calc(100vw - 70px) 3/4`', () => {
    expect(
      sizesToPicture(
        '(min-width: 640px)  calc(33.33vw - 70px - 2vw) 3/4, calc(100vw - 70px) 3/4',
        fullAspectRatioSource,
      ),
    ).toMatchObject({
      img: {
        sizes: '(min-width: 640px) calc(33.33vw - 70px - 2vw), calc(100vw - 70px)',
        srcset: '3/4.jpg 100w',
      },
    })
  })

  it('transforms `(min-width: 640px)   100vw 1000/534, 100vw 1000/835`', () => {
    expect(
      sizesToPicture('(min-width: 640px)   100vw 1000/534, 100vw 1000/835', fullAspectRatioSource),
    ).toMatchObject({
      img: { media: '', sizes: '100vw', srcset: '4/3.jpg 100w' },
      sources: [
        { media: '(min-width: 640px)', sizes: '(min-width: 640px) 100vw', srcset: '16/9.jpg 100w' },
      ],
    })
  })

  it('transforms `100vw 100vh`', () => {
    expect(sizesToPicture('100vw 100vh', fullAspectRatioSource)).toMatchObject({
      img: { sizes: '100vw', srcset: '16/9.jpg 100w' },
      sources: [
        {
          media: '(max-aspect-ratio: 656/1000)',
          sizes: '100vw',
          srcset: '9/16.jpg 100w',
        },
        {
          media: '(max-aspect-ratio: 875/1000)',
          sizes: '100vw',
          srcset: '3/4.jpg 100w',
        },
        {
          media: '(max-aspect-ratio: 1167/1000)',
          sizes: '100vw',
          srcset: '1/1.jpg 100w',
        },
        {
          media: '(max-aspect-ratio: 1556/1000)',
          sizes: '100vw',
          srcset: '4/3.jpg 100w',
        },
      ],
    })
  })

  it('transforms `(min-width: 1024px) 100vw 100vh, (min-width: 640px) 100vw 70vh, 100vw 375px`', () => {
    expect(
      sizesToPicture(
        '(min-width: 1024px) 100vw 100vh, (min-width: 640px) 100vw 70vh, 100vw 375px',
        fullAspectRatioSource,
      ),
    ).toMatchObject({
      img: {
        sizes: '(min-width: 1024px) 100vw, (min-width: 640px) 100vw, 100vw',
        srcset: '16/9.jpg 100w',
      },
      sources: [
        {
          media:
            '(min-width: 1024px) and (max-aspect-ratio: 656/1000), (min-width: 640px) and (max-aspect-ratio: 459/1000), (max-width: 246px)',
          sizes: '(min-width: 1024px) 100vw, (min-width: 640px) 100vw, 100vw',
          srcset: '9/16.jpg 100w',
        },
        {
          media:
            '(min-width: 1024px) and (max-aspect-ratio: 875/1000), (min-width: 640px) and (max-aspect-ratio: 612/1000), (max-width: 328px)',
          sizes: '(min-width: 1024px) 100vw, (min-width: 640px) 100vw, 100vw',
          srcset: '3/4.jpg 100w',
        },
        {
          media:
            '(min-width: 1024px) and (max-aspect-ratio: 1167/1000), (min-width: 640px) and (max-aspect-ratio: 817/1000), (max-width: 437px)',
          sizes: '(min-width: 1024px) 100vw, (min-width: 640px) 100vw, 100vw',
          srcset: '1/1.jpg 100w',
        },
        {
          media:
            '(min-width: 1024px) and (max-aspect-ratio: 1556/1000), (min-width: 640px) and (max-aspect-ratio: 1089/1000), (max-width: 583px)',
          sizes: '(min-width: 1024px) 100vw, (min-width: 640px) 100vw, 100vw',
          srcset: '4/3.jpg 100w',
        },
      ],
    })
  })
})
