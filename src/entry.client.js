import Vue from 'vue'
import flatMap from 'lodash-es/flatMap'
import isArray from 'lodash-es/isArray'
import config from '../config/config.client'
import { API_URL } from './core/catalog'
import http from './core/services/http/http.client'
import createApp from './create-app'
import createDiContainer from './services/create-di-container'

const container = createDiContainer()

container.constant(API_URL, config.apiUrl)

container.register(http)

const { app, store, router } = createApp(container)

// Add router hook for handling asyncData.
// Using router.beforeResolve() so that all async components are resolved.
// Doing it after initial route is resolved so that we don't double-fetch
// the data that we already have (provided via SSR).
router.beforeResolve((to, from, next) => {
  loadAsyncData(to).then(next, next)
})

router.onError(err => {
  console.error('Route error:', err)
})

if (window.__INITIAL_STATE__) {
  store.replaceState(window.__INITIAL_STATE__)

  app.$mount('#app')
} else if (process.env.NODE_ENV !== 'production') {
  // We are in a non-ssr context, this can only happen in dev mode (e.g. dev server without SSR)

  // create mount point
  const mountPoint = document.createElement('div')
  mountPoint.id = 'app'
  document.body.appendChild(mountPoint)

  // Run asyncData for starting route
  loadAsyncData(router.currentRoute).then(() => app.$mount(`#${mountPoint.id}`))
}

if (navigator.serviceWorker && window.location.protocol === 'https:') {
  navigator.serviceWorker.register('/service-worker.js')
}

function loadAsyncData(route) {
  const matched = router.getMatchedComponents(route)
  // const prevMatched = router.getMatchedComponents(from)

  // let diffed = false
  const activated = matched
  // matched.filter(
  //   (c, i) => diffed || (diffed = prevMatched[i] !== c)
  // );

  if (!activated.length) {
    return Promise.resolve()
  }

  // start loading indicator
  return Promise.all(
    flatMap(activated, Component => {
      const { asyncData } = Vue.extend(Component).options

      return (
        asyncData &&
        (isArray(asyncData) ? asyncData : [asyncData]).map(request =>
          request({
            store,
            route,
          }),
        )
      )
    }),
  )
}
